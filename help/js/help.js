Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

(function(){
  var app = angular.module('bio_helper', ["ngRoute", "ngSanitize"]);

  // app.controller("score_controller", function($scope, $routeParams, $location, $http){
  //   $scope.scoreInfo = [];
  //   $scope.findData = function() {

  //       $scope.query = $location.search();
  //       $http({
  //           url: "/lyrics/api/score.php", 
  //           method: "GET",
  //           params: {id: $scope.query.id}
  //       }).success(function(data, status, headers, config) {
  //           $scope.scoreInfo = data;
  //       });
  //   };

  //   $scope.$on('$locationChangeStart', function(event) {
  //       $scope.findData();
  //   });

  //   $scope.findData();
  // });
  app.controller("bio_controller", function($scope, $routeParams, $location, $http, $timeout){
      $scope.biodata = new Object();

      $scope.biodata.text_type = "artist"

      $scope.biodata.f_name = "";
      $scope.biodata.m_name = "";
      $scope.biodata.l_name = "";
      $scope.biodata.born = "";
      $scope.biodata.died= "";
      $scope.biodata.bio_text = "";

      $scope.biodata.sources = new Array(1);

      $scope.biojson = "";

      $scope.addSource = function() {
        $scope.biodata.sources.push("");
      };

      $scope.removeSource = function(index) {
        $scope.biodata.sources.remove(index);
      };

      $scope.submit = function() {
        $scope.biojson = JSON.stringify($scope.biodata);
        
        $.post("./php/bio_import.php", $scope.biodata).done(function(data) {
          alert(data);
        });
      };
  });

  app.controller("pub_controller", function($scope){
      $scope.biodata = new Object();

      $scope.biodata.text_type = "publisher"

      $scope.biodata["name"] = "";
      $scope.biodata.city = "";
      $scope.biodata.address = "";
      $scope.biodata.start_year = "";
      $scope.biodata.end_year = "";
      $scope.biodata.bio_text = "";

      $scope.biodata.sources = new Array(1);

      $scope.biojson = "";

      $scope.addSource = function() {
        $scope.biodata.sources.push("");
      };

      $scope.removeSource = function(index) {
        $scope.biodata.sources.remove(index);
      };

      $scope.submit = function() {
        $scope.biojson = JSON.stringify($scope.biodata);
        
        $.post("./php/bio_import.php", $scope.biodata).done(function(data) {
          alert(data);
        });
      };
  });

})()