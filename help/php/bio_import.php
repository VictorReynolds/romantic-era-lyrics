<?php
	/* This is a helper file that saves json data from the
	 * help html pages and saves them in this directory.
	 * This is so that interns can save biographies to the server
	 * in a format that's simple to import.
	 *
	*/
	if (isset($_POST["text_type"]) && $_POST["text_type"] == "artist")
	{
		$path = "./bio_texts/";
		if (isset($_POST["f_name"]) && $_POST["f_name"] != "")
		{
			$name = preg_replace("/[^A-Za-z0-9 ]/", '', $_POST["f_name"]) . "_" . preg_replace("/[^A-Za-z0-9 ]/", '', $_POST["l_name"]);
		}
		elseif (isset($_POST["l_name"]) && $_POST["l_name"] != "")
		{
			$name = preg_replace("/[^A-Za-z0-9 ]/", '', $_POST["l_name"]);
		}
		else
		{
			echo("Name not entered correctly.");
			exit();
		}
	}
	elseif (isset($_POST["text_type"]) && $_POST["text_type"] == "publisher")
	{
		$path = "./pub_texts/";
		if(isset($_POST["name"]) && $_POST["name"] != "" && isset($_POST["city"]) && $_POST["city"] != "")
		{
			$name = preg_replace("/[^A-Za-z0-9 ]/", '', $_POST["city"]) . "_" . preg_replace("/[^A-Za-z0-9 ]/", '', $_POST["name"]);
		}
	}

	// Create the directory to store the data if it doesn't already exist
	if (!file_exists($path)) {
		mkdir($path, 0777, true);
	}

	unset($_POST["text_type"]);

	$f = './bio_texts/';
    $io = popen ( '/usr/bin/du -sk ' . $f, 'r' );
    $size = fgets ( $io, 4096);
    $size = substr ( $size, 0, strpos ( $size, "\t" ) );
    
    if ($size < 256) 
    {
		$myfile = fopen($path . $name . ".json", "w") or die("Unable to open file!");
		fwrite($myfile, json_encode($_POST));
		fclose($myfile);

		echo("Sucessfully saved " . $name . " description file. Directory size is: " . $size);
    }
    else
    {
    	echo("Directory size too large. Email Victor.");
    }
?>