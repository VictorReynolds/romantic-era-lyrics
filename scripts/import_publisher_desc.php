<?php
	$pub_text_path = "./pub_texts/";

	require_once("../conf/config.php");

	function addPublisherDescription($pub_data)
	{
		global $db;

		$find_sql = "SELECT * FROM Publisher WHERE city LIKE CONCAT('%', :city, '%') AND name LIKE CONCAT('%', :name, '%')";
		$params_find_sql = array("city"=>$pub_data["city"], "name"=>$pub_data["name"]);

		$return = $db->prepare($find_sql);
		$return->execute($params_find_sql);
		$return = $return->fetchAll(PDO::FETCH_ASSOC);

		if (count($return) == 1)
		{
			$bio_sources = implode($pub_data["sources"], "~");
			$update_sql = "UPDATE Publisher SET start_year=:start_year, end_year=:end_year, description=:bio_text, description_sources=:sources, address=:address WHERE id=:id";
			$params_update_sql = array("start_year"=>$pub_data["start_year"], "end_year"=>$pub_data["end_year"], "bio_text"=>$pub_data["bio_text"], "id"=>$return[0]["id"], "sources"=>$bio_sources, "address"=>$pub_data["address"]);

			$update = $db->prepare($update_sql);
			$update->execute($params_update_sql);
			echo("Successfully imported description for " . $pub_data["city"] . ": " . $pub_data["name"] . "\n\n");
		}
	}

	$pub_files = scandir($pub_text_path);

	foreach ($pub_files as $index=>$file)
	{
		if ($index > 1)
		{
			$import_file = file_get_contents($pub_text_path . $file);

			addPublisherDescription(json_decode($import_file, true));
		}
	}
?>