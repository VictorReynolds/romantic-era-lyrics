<?php
	require_once("lib/API_Utilities.php");

	if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["id"])) {
		$returnArray = array();
		$sql_param_workId = array(":work_id"=>$_GET["id"]);


		$sql_work = "SELECT * FROM Work WHERE id=:work_id";

		$work = fetchData($sql_work, $sql_param_workId);

		if (!empty($work)) {
			$returnArray["work"] = $work[0];

			$sql_lyricist = "SELECT * FROM Artist INNER JOIN Artist_has_Work on Artist.id = Artist_id WHERE Artist_has_Work.Work_id=:work_id";

			$lyricist = fetchData($sql_lyricist, $sql_param_workId);
			$returnArray["lyricist"] = $lyricist;

			$sql_score = "SELECT * FROM Score WHERE work_id=:work_id";
			$score = fetchData($sql_score, $sql_param_workId);
			$returnArray["score"] = $score;

			foreach($returnArray["score"] as &$item)
			{
				$imageArray = findImagesFromScoreId($item["id"], $item);

				$item["thumb"] = $imageArray["thumbs"][0];
			}
		}

		echo(json_encode($returnArray));
	}