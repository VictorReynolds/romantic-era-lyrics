<?php
	require_once("lib/API_Utilities.php");

	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		$returnArray = array();

		$sql_score_count = "SELECT Count(*) as score_count FROM Score";
		$score_count_result = fetchData($sql_score_count);

		$returnArray['score_count'] = $score_count_result[0]['score_count'];

		$sql_collection_count = "SELECT Count(*) as collection_count FROM Collection";
		$collection_count_result = fetchData($sql_collection_count);

		$returnArray['collection_count'] = $collection_count_result[0]['collection_count'];

		$sql_publisher_count = "SELECT Count(*) as publisher_count FROM Publisher";
		$publisher_count_result = fetchData($sql_publisher_count);

		$returnArray['publisher_count'] = $publisher_count_result[0]['publisher_count'];

		$sql_lyricist_count = "SELECT COUNT(*) as lyricist_count FROM (SELECT DISTINCT Artist_id FROM Artist_has_Work) as lyricists";
		$lyricist_count_result = fetchData($sql_lyricist_count);

		$returnArray['lyricist_count'] = $lyricist_count_result[0]['lyricist_count'];
		
		$sql_composer_count = "SELECT COUNT(*) as composer_count FROM (SELECT DISTINCT Artist_id FROM Artist_has_Score) as composers";
		$composer_count_result = fetchData($sql_composer_count);

		$returnArray['composer_count'] = $composer_count_result[0]['composer_count'];

		header("Content-Type: application/json");
		echo(json_encode($returnArray));
	}
?>