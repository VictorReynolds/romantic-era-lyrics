<?php
	require_once("lib/API_Utilities.php");

	if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["id"])) {
		$returnArray = array();
		$sql_param_collectionId = array(":collection_id"=>$_GET["id"]);


		$sql_collection = "SELECT * FROM Collection WHERE id=:collection_id";

		$collection = fetchData($sql_collection, $sql_param_collectionId);

		$returnArray["collection"] = $collection[0];

		$sql_publishers = "SELECT Publisher.* FROM Publisher_has_Collection INNER JOIN Publisher ON Publisher.id = Publisher_has_Collection.Publisher_id WHERE Collection_id=:collection_id";
		$publishers = fetchData($sql_publishers, $sql_param_collectionId);
		$returnArray["publisher"] = $publishers;

		$sql_scores = "SELECT Score.*,index_start FROM Score INNER JOIN Collection_has_Score ON Score.id = Collection_has_Score.Score_id WHERE Collection_id=:collection_id ORDER BY index_start";
		$scores = fetchData($sql_scores, $sql_param_collectionId);
		$returnArray["scores"] = $scores;

		foreach($returnArray["scores"] as &$item)
		{
			$imageArray = findImagesFromScoreId($item["id"], $item);

			$item["thumb"] = $imageArray["thumbs"][0];
		}

		sanitizeEmptyStrings($returnArray);

		header("Content-Type: application/json");
		echo(json_encode($returnArray));
	}
?>