<?php
	require_once("lib/API_Utilities.php");

	if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["id"])) {
		$returnArray = array();
		$sql_param_artistId = array(":artist_id"=>$_GET["id"]);


		$sql_artist = "SELECT * FROM Artist WHERE id=:artist_id";
		$artist = fetchData($sql_artist, $sql_param_artistId);

		if (!empty($artist)) {
			$returnArray["artist"] = $artist[0];

			$sql_work = "SELECT *, (SELECT COUNT(*) From Score Where Score.Work_id = Work.id) as score_count," .
			            " (SELECT id FROM Score WHERE Score.work_id = Work.id LIMIT 1) as first_score_id" .
			            " FROM Work INNER JOIN Artist_has_Work ON Work.id = Work_id WHERE Artist_id=:artist_id ORDER BY Work.title";
			$work = fetchData($sql_work, $sql_param_artistId);
			$returnArray["work"] = $work;

			// Insert thumbnail link for score corresponding to first_score_id for our work
			foreach($returnArray["work"] as &$item)
			{
				$imageArray = findImagesFromScoreId($item["first_score_id"]);

				$item["thumb"] = $imageArray["thumbs"][0];
			}

			$sql_score = "SELECT * FROM Score INNER JOIN Artist_has_Score ON Score.id = Score_id WHERE Artist_id=:artist_id ORDER BY Score.title";
			$score = fetchData($sql_score, $sql_param_artistId);
			$returnArray["score"] = $score;

			// Insert thumbnail link as property "thumb" for each score
			foreach($returnArray["score"] as &$item)
			{
				$imageArray = findImagesFromScoreId($item["id"], $item);

				$item["thumb"] = $imageArray["thumbs"][0];
			}
		}

		sanitizeEmptyStrings($returnArray);

		// Fix the quotes so that hyperlinks, etc aren't broken...
		$returnArray["artist"]["biography_sources"] = replaceMicrosoftQuotes($returnArray["artist"]["biography_sources"]);
		header("Content-Type: application/json");
		echo(json_encode($returnArray));
	}
?>