<?php
	require_once("lib/API_Utilities.php");

	if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["id"])) {
		$returnArray = array();
		$sql_param_publisherId = array(":publisher_id"=>$_GET["id"]);


		$sql_publisher = "SELECT * FROM Publisher WHERE id=:publisher_id";
		$publisher = fetchData($sql_publisher, $sql_param_publisherId);

		if (!empty($publisher)) {
			$returnArray["publisher"] = $publisher[0];

			$sql_scores = "SELECT Score.* FROM Score INNER JOIN Publisher_has_Score ON Score.id = Publisher_has_Score.Score_id WHERE Publisher_id = :publisher_id";

			$scores = fetchData($sql_scores, $sql_param_publisherId);
			$returnArray["scores"] = $scores;

			foreach($returnArray["scores"] as &$item)
			{
				$imageArray = findImagesFromScoreId($item["id"], $item);

				$item["thumb"] = $imageArray["thumbs"][0];
			}

			$sql_composers = "SELECT DISTINCT Artist.* FROM Artist INNER JOIN Artist_has_Score ON Artist.id = Artist_has_Score.Artist_id " . 
			                 "INNER JOIN Publisher_has_Score ON Publisher_has_Score.Score_id = Artist_has_Score.Score_id WHERE Publisher_id = :publisher_id";

			$composers = fetchData($sql_composers, $sql_param_publisherId);
			$returnArray["composers"] = $composers;

			$sql_lyricists = "SELECT Artist.* FROM Score INNER JOIN Publisher_has_Score ON Score.id = Publisher_has_Score.Score_id INNER JOIN Work ON " .
			                 "Work.id = Score.Work_id INNER JOIN Artist_has_Work on Work.id = Artist_has_Work.Work_id INNER JOIN Artist ON Artist.id = Artist_has_Work.Work_id " .
			                 "WHERE Publisher_has_Score.Publisher_id = :publisher_id";
			$lyricists = fetchData($sql_lyricists, $sql_param_publisherId);
			$returnArray["lyricists"] = $lyricists;
		}

		sanitizeEmptyStrings($returnArray);

		header('Content-Type: application/json');
		echo(json_encode($returnArray));
	}
?>