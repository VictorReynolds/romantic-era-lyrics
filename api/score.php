<?php
	require_once("lib/API_Utilities.php");

	if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["id"])) {
		$returnArray = array();
		$sql_param_scoreId = array(":score_id"=>$_GET["id"]);


		$sql_score = "SELECT * FROM Score WHERE id=:score_id";

		$score = fetchData($sql_score, $sql_param_scoreId);

		if (!empty($score)) {
			$returnArray["score"] = $score[0];

			$returnArray["augnotes"] = array();

			$sql_augnotes = "SELECT * FROM Augnotes WHERE Score_id = :score_id";
			$augnotes = fetchData($sql_augnotes, $sql_param_scoreId);
			$returnArray["augnotes"] = $augnotes;

			foreach ($returnArray["augnotes"] as &$aug)
			{
				$decode = json_decode($aug["data"]);
				$aug["data"] = $decode;
				$aug["mp3_path"] = BASE_URL . $aug["mp3_path"];
			}

			$imageArray = findImagesFromScoreId($score[0]["id"], $score[0]);

			$returnArray["images"] = $imageArray["images"];
			$returnArray["thumbs"] = $imageArray["thumbs"];


			// Extra logic to return zip file containing jpeg images of scores
			// for the "Download JPEG Images" link.
			if (isset($_GET["jpeg_archive"]) && $_GET["jpeg_archive"] == 1) {
				$zip = new ZipArchive();

				// Create the directory to store the data if it doesn't already exist
				if (!file_exists("./archives/")) {
					mkdir("./archives", 0777, true);
				}

				// Filename for the zip file in the directory on the server
				$filename = "./archives/" . $returnArray["score"]["ref_id"] . getmygid() . ".zip";

				// Filename for what the user will actually see in his/her "downloads" folder
				$user_filename = preg_replace('/[^A-Za-z0-9 ]/', '', $returnArray["score"]["title"]);
				$user_filename = str_replace(' ', '_', $user_filename) . '_jpegs.zip';

			    if ($zip->open($filename, ZIPARCHIVE::CREATE )!==TRUE) {
    				exit("cannot open <$archive_file_name>\n");
				}

				foreach($returnArray["images"] as $image)
				{
					$image_filename_exploded = explode("/", $image);
					$image_filename_exploded = array_slice($image_filename_exploded, 2);
					$image_filename = implode("/", $image_filename_exploded);

					$zip->addFile(ROOT_PATH . $image_filename, $image_filename_exploded[count($image_filename_exploded) - 1]);
				}
				$zip->close();
				header("Content-type: application/zip"); 
				header("Content-Disposition: attachment; filename=" . $user_filename); 
				header("Content-length: " . filesize($filename));
				header("Pragma: no-cache"); 
				header("Expires: 0"); 
				readfile($filename);

				unlink($filename);
				exit;
			}

			$sql_lyricist = "SELECT * FROM Artist INNER JOIN Artist_has_Work ON Artist.id = Artist_id WHERE Work_id=:work_id";
			$lyricist = fetchData($sql_lyricist, array(":work_id"=>$score[0]["work_id"]));
			$returnArray["lyricist"] = $lyricist;

			$sql_composer = "SELECT * FROM Artist INNER JOIN Artist_has_Score ON Artist.id = Artist_id WHERE Score_id=:score_id";
			$composer = fetchData($sql_composer, $sql_param_scoreId);
			$returnArray["composer"] = $composer;

			$sql_publisher = "SELECT * FROM Publisher INNER JOIN Publisher_has_Score ON Publisher.id = Publisher_id WHERE Score_id=:score_id";
			$publisher = fetchData($sql_publisher, $sql_param_scoreId);
			$returnArray["publisher"] = $publisher;

			$sql_collection = "SELECT * FROM Collection INNER JOIN Collection_has_Score on Collection.id = Collection_id WHERE Score_id=:score_id";
			$collection = fetchData($sql_collection, $sql_param_scoreId);
			$returnArray["collection"] = $collection;
		}

		sanitizeEmptyStrings($returnArray);

		header("Content-Type: application/json");
		echo(json_encode($returnArray));
	}
?>