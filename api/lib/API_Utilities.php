<?php
	/* API_Utilities.php
	 * Author: Victor Reynolds
	 *
	 * A collection of miscellaneous functions used throughout the API files.
	*/
	require_once("../conf/config.php");

	// A shortcut function that returns a PDO object based on a $sql_satement string
	// and an optional parameters associative array
	function fetchData($sql_statement, $sql_params = NULL)
	{
		global $db;
		$return = $db->prepare($sql_statement);
		if ($sql_params) {
			$return->execute($sql_params);
		}
		else {
			$return->execute();
		}
		return $return->fetchAll(PDO::FETCH_ASSOC);
	}

	// This function takes an associative array and replaces any empty
	// string values with null values.
	function sanitizeEmptyStrings(&$item)
	{
		if (gettype($item) == "string" && trim($item) == "") {
			$item = NULL;
		}
		else if (gettype($item) == "array")
		{
			foreach($item as &$child)
			{
				sanitizeEmptyStrings($child);
			}
		}
	}

	// A function to replace microsoft's annoying auto left/right quotes
	// in word docs. This is used to fix the links to outside sources
	// in publisher descriptions and artist bios.
	function replaceMicrosoftQuotes($str)
	{
		$quotes = array(
		    "\xC2\xAB"     => '"', // « (U+00AB) in UTF-8
		    "\xC2\xBB"     => '"', // » (U+00BB) in UTF-8
		    "\xE2\x80\x98" => "'", // ‘ (U+2018) in UTF-8
		    "\xE2\x80\x99" => "'", // ’ (U+2019) in UTF-8
		    "\xE2\x80\x9A" => "'", // ‚ (U+201A) in UTF-8
		    "\xE2\x80\x9B" => "'", // ‛ (U+201B) in UTF-8
		    "\xE2\x80\x9C" => '"', // “ (U+201C) in UTF-8
		    "\xE2\x80\x9D" => '"', // ” (U+201D) in UTF-8
		    "\xE2\x80\x9E" => '"', // „ (U+201E) in UTF-8
		    "\xE2\x80\x9F" => '"', // ‟ (U+201F) in UTF-8
		    "\xE2\x80\xB9" => "'", // ‹ (U+2039) in UTF-8
		    "\xE2\x80\xBA" => "'", // › (U+203A) in UTF-8
		);

		return strtr($str, $quotes);
	}

	/* This function is used to get all of the images from a score given the refId and the number of pages.
	* 
	* $indexStart must be defined for collections where the scores must be split up
	*
	* This function will return an array with two subarrays:
	* "images" is a list of links to the large images of a score
	* "thumbs" is a list of the score's thumbnails
	* TODO: Add 'tiffs' array for tiff images if the server can handle them
	*/
	function findImagesFromRefId($path, $refId, $pageCount, $indexStart = 1)
	{
		$returnArray = array();
		$returnArray["images"] = array();
		$returnArray["thumbs"] = array();
		//$returnArray["tiffs"] = array();

		$thumbsExist = file_exists(ROOT_PATH . "score_images/" . $path . "thumbs/");

		for ($pagenum = 1; $pagenum < $pageCount + 1; $pagenum++)
		{
			$numFormatted = sprintf('%03d', ($indexStart - 1) + $pagenum);
			array_push($returnArray["images"], IMG_ROOT . $path . "jpeg/" . $refId. $numFormatted . ".jpg");
			if ($thumbsExist) {
				array_push($returnArray["thumbs"], IMG_ROOT . $path . "thumbs/" . $refId . $numFormatted . "_t.jpg");
			}
		}

		if (!$thumbsExist)
		{
			$returnArray["thumbs"] = $returnArray["images"];
		}

		return $returnArray;
	}

	/* This function finds determines whether or not a score
	 * belongs to a collection and where to look for the score images.
	 * It then calls the findImagesFromRefId function above
	 * once it has been determined whether this score belongs to a collection
	 * or is an individual piece.
	 *
	 * $score is an optional parameter representing an associative array
	 * for an individual score
	 *
	*/
	function findImagesFromScoreId($scoreId, $score = null)
	{
		$sql_param_scoreId = array(":score_id"=>$scoreId);
		
		if ($score == null) {
			$sql_score = "SELECT * FROM Score WHERE id=:score_id";
			$score = fetchData($sql_score, $sql_param_scoreId);

			if (!empty($score))
			{
				$score = $score[0];
			}
		}

		if (!empty($score)) {
			$returnArray["score"] = $score;

			$sql_collection = "SELECT Collection.ref_id, Collection.title, Collection.id, Collection_has_Score.index_start FROM Collection INNER JOIN Collection_has_Score ON Collection.id = Collection_id INNER JOIN Score ON Score.id = Score_id WHERE Score_id=:score_id";
			$collection = fetchData($sql_collection, $sql_param_scoreId);
			$returnArray["collection"] = $collection;

			// When selecting images, a score will be in the 'individual' folder if it's not part of a collection
			$subDir = "";
			$refIdDir;
			$indexStart;
			if (empty($collection))
			{
				$subDir = "individual/";
				$refIdDir = $returnArray["score"]["ref_id"];
				$indexStart = 1;
			}
			else
			{
				$refIdDir = $collection[0]["ref_id"];
				$indexStart = $collection[0]["index_start"];
			}

			$imageArray = findImagesFromRefId($subDir . $refIdDir . "/", $refIdDir, $returnArray["score"]["page_count"], $indexStart);

			return $imageArray;
		}
	}

	// This function finds the thumbnail for a collection given the collection ref_id
	// The thumbnail should just be saved as "thumb.jpg" in the collection folder.
	function find_collection_thumbnail($ref_id)
	{
		// Check physical file location
		if (file_exists(ROOT_PATH . "score_images/" . $ref_id . "/thumb.jpg")) {
			// Return URL
			return IMG_ROOT . "/" . $ref_id . "/thumb.jpg";
		}
		return "";
	}

	// This selects a thumbnail from a publisher's collection of sheet music
	// The selected thumbnail is simply the first one returned by the query.
	// If a more predictable order is desired, one order the query by title
	function find_publisher_thumbnail($id)
	{
		$sql_param_publisherId = array(":publisher_id"=>$id);
		$sql_publisher_scores = "SELECT * FROM Score INNER JOIN Publisher_has_Score ON Score.id = Publisher_has_Score.Score_id WHERE Publisher_has_Score.Publisher_id = :publisher_id";

		$publisher_scores = fetchData($sql_publisher_scores, $sql_param_publisherId);

		if (!empty($publisher_scores))
		{
			$imageArray = findImagesFromScoreId($publisher_scores[0]["id"], $publisher_scores[0]);

			if (!empty($imageArray["thumbs"]))
			{
				return $imageArray["thumbs"][0];
			}
		}
		return "";
	}

	/* Similar to the above publisher function, this function selects a score thumbnail
	 * for a score that an artist composed - if none are found, then it attempts to select
	 * one from the scores corresponding to works that an artist wrote.
	 *
	 *
	*/
	function find_artist_thumbnail($id, $score_count, $work_count)
	{
		$sql_param_artist_id = array(":artist_id"=>$id);

		if ($score_count > 0) {
			$sql_artist_scores = "SELECT * FROM Score INNER JOIN Artist_has_Score ON Artist_has_Score.Score_id = Score.id WHERE Artist_has_Score.Artist_id = :artist_id";
		}
		else
		{
			$sql_artist_scores = "SELECT Score.* FROM Score LEFT OUTER JOIN Work ON Score.Work_id = Work.id INNER JOIN Artist_has_Work ON Artist_has_Work.Work_id = Work.id WHERE Artist_has_Work.Artist_id = :artist_id";
		}

		$artist_scores = fetchData($sql_artist_scores, $sql_param_artist_id);;

		if (!empty($artist_scores))
		{
			$imageArray = findImagesFromScoreId($artist_scores[0]["id"], $artist_scores[0]);

			if (!empty($imageArray["thumbs"]))
			{
				return $imageArray["thumbs"][0];
			}
		}

		return "";
	}
?>