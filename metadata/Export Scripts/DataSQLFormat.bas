Attribute VB_Name = "Module1"
Sub FillFunctionNames()
    'Options:
    ShowColumnTitles = True
    
    'Source cells in the spreadsheet for a score
    SourceTitle = "A2"
    SourceArtists = "B2"
    SourceDedicatee = "C2"
    SourcePublisher = "D2"
    SourceDate = "E2"
    SourceSubject = "G2"
    SourceForm = "H2"
    SourceKeySig = "I2"
    SourceTimeSig = "J2"
    SourceTempo = "K2"
    SourceInstr = "L2"
    SourceCoverDesc = "M2"
    SourceNote = "V2"
    
    'Columns for the respective fields in the "Data" spreadsheet - the one to be exported.
    ColRefId = "A"
    ColTitle = "B"
    ColLyricist = "C"
    ColComposer = "D"
    ColAuthor = "E"
    ColDedicatee = "F"
    ColPublisher = "G"
    ColDate = "H"
    ColSubject = "I"
    ColForm = "J"
    ColKeySig = "K"
    ColTimeSig = "L"
    ColTempo = "M"
    ColInstr = "N"
    ColCoverDesc = "O"
    ColNote = "P"
    ColPageCount = "Q"


    Dim sheetCount
    Dim sheetStartIndex
    If Worksheets(1).Name = "MasterSheet" Then
        sheetCount = Worksheets.Count - 1
        sheetStartIndex = 2
    Else
        sheetCount = Worksheets.Count
        sheetStartIndex = 1
    End If
    
    'RefId Array - user identifier for score
    Dim ListRefId
    ListRefId = Array()
    ReDim ListRefId(sheetCount)
    
    'List of score titles
    Dim ListTitle
    ListTitle = Array()
    ReDim ListTitle(sheetCount)
    
    'List of lyricists
    Dim ListLyricist
    ListLyricist = Array()
    ReDim ListLyricist(sheetCount)
    
    'List of composers
    Dim ListComposer
    ListComposer = Array()
    ReDim ListComposer(sheetCount)
    
    'List of authors
    Dim ListAuthor
    ListAuthor = Array()
    ReDim ListAuthor(sheetCount)
    
    'List of publishers
    Dim ListPublisher
    ListPublisher = Array()
    ReDim ListPublisher(sheetCount)
    
    Dim ListDedicatee
    ListDedicatee = Array()
    ReDim ListDedicatee(sheetCount)
    
    Dim ListDate
    ListDate = Array()
    ReDim ListDate(sheetCount)
    
    Dim ListSubject
    ListSubject = Array()
    ReDim ListSubject(sheetCount)
    
    'List of forms/genres
    Dim ListForm
    ListForm = Array()
    ReDim ListForm(sheetCount)
    
    'List of key signatures
    Dim ListKeySig
    ListKeySig = Array()
    ReDim ListKeySig(sheetCount)
    
    'List of time signatures
    Dim ListTimeSig
    ListTimeSig = Array()
    ReDim ListTimeSig(sheetCount)
    
    Dim ListTempo
    ListTempo = Array()
    ReDim ListTempo(sheetCount)
    
    'List of instrumentations
    Dim ListInstr
    ListInstr = Array()
    ReDim ListInstr(sheetCount)
    
    'List of cover descriptions
    Dim ListCoverDesc
    ListCoverDesc = Array()
    ReDim ListCoverDesc(sheetCount)
    
    Dim ListNote
    ListNote = Array()
    ReDim ListNote(sheetCount)
    
    Dim ListPageCount
    ListPageCount = Array()
    ReDim ListPageCount(sheetCount)
    
    For i = sheetStartIndex To Worksheets.Count()
        Dim listIndex
        listIndex = i + 1 - sheetStartIndex
        
        ListRefId(listIndex) = Worksheets(i).Name
        
        Worksheets(i).Select
        
        ListTitle(listIndex) = Range(SourceTitle)
        ListPublisher(listIndex) = Range(SourcePublisher)
        ListDedicatee(listIndex) = Range(SourceDedicatee)
        ListDate(listIndex) = Range(SourceDate)
        ListSubject(listIndex) = Range(SourceSubject)
        ListForm(listIndex) = Range(SourceForm)
        ListKeySig(listIndex) = Range(SourceKeySig)
        ListTimeSig(listIndex) = Range(SourceTimeSig)
        ListTempo(listIndex) = Range(SourceTempo)
        ListInstr(listIndex) = Replace(LCase(Replace(Range(SourceInstr), " ", "")), ";", ",")
        ListCoverDesc(listIndex) = Range(SourceCoverDesc)
        ListNote(listIndex) = Range(SourceNote)
        
        'One cell has the composer, lyricist, and author
        Dim contribList
        contribList = Split(Range(SourceArtists), ";")
        
        For Each artist In contribList
            If InStr(artist, "(composer)") <> 0 Then
                artist = Trim(Replace(artist, "(composer)", ""))
                If ListComposer(listIndex) & "" <> "" Then
                    artist = ";" & artist
                End If
                ListComposer(listIndex) = ListComposer(listIndex) & artist
            ElseIf InStr(artist, "(lyricist)") <> 0 Then
                artist = Trim(Replace(artist, "(lyricist)", ""))
                If ListLyricist(listIndex) & "" <> "" Then
                    artist = ";" & artist
                End If
                ListLyricist(listIndex) = ListLyricist(listIndex) & artist
            ElseIf InStr(artist, "(author)") <> 0 Then
                artist = Trim(Replace(artist, "(author)", ""))
                If ListAuthor(listIndex) & "" <> "" Then
                    artist = ";" & artist
                End If
                ListAuthor(listIndex) = ListAuthor(listIndex) & artist
            End If
            
            'Figure out the page count
            Dim pageCount
            pageCount = 0
            Do While Range("A" & (3 + pageCount)) & "" <> ""
                pageCount = pageCount + 1
            Loop
            ListPageCount(listIndex) = pageCount
        Next
        
    Next
    
    Sheets.Add After:=Worksheets(1)
    Set NewSheet = Worksheets(2)
    NewSheet.Select
    NewSheet.Name = "Data"

    Dim rowStartIndex
    
    If ShowColumnTitles Then
        rowStartIndex = 1
        
        Range(ColRefId & 1).Select
        ActiveCell.Value = "ref_id"
        
        Range(ColTitle & 1).Select
        ActiveCell.Value = "title"
        
        Range(ColLyricist & 1).Select
        ActiveCell.Value = "lyricist"
        
        Range(ColComposer & 1).Select
        ActiveCell.Value = "composer"
        
        Range(ColAuthor & 1).Select
        ActiveCell.Value = "author"
        
        Range(ColDedicatee & 1).Select
        ActiveCell.Value = "dedicatee"
        
        Range(ColPublisher & 1).Select
        ActiveCell.Value = "publisher"
        
        Range(ColDate & 1).Select
        ActiveCell.Value = "year"
        
        Range(ColSubject & 1).Select
        ActiveCell.Value = "subject"
        
        Range(ColForm & 1).Select
        ActiveCell.Value = "form"
        
        Range(ColKeySig & 1).Select
        ActiveCell.Value = "key_sig"
        
        Range(ColTimeSig & 1).Select
        ActiveCell.Value = "time_sig"
        
        Range(ColTempo & 1).Select
        ActiveCell.Value = "tempo"
        
        Range(ColInstr & 1).Select
        ActiveCell.Value = "instruments"
        
        Range(ColCoverDesc & 1).Select
        ActiveCell.Value = "cover_desc"
        
        Range(ColNote & 1).Select
        ActiveCell.Value = "note"
        
        Range(ColPageCount & 1).Select
        ActiveCell.Value = "page_count"
    Else
        rowStartIndex = 0
    End If
    
    Range("A1", "P" & sheetCount + 1).Select
    Selection.NumberFormat = "@"
    
    For i = 1 To UBound(ListRefId)
        Dim colIndex
        colIndex = i + rowStartIndex
        
        Range(ColRefId & colIndex).Select
        ActiveCell.FormulaR1C1 = ListRefId(i)
        
        Range(ColTitle & colIndex).Select
        ActiveCell.FormulaR1C1 = ListTitle(i)
        
        Range(ColLyricist & colIndex).Select
        ActiveCell.FormulaR1C1 = ListLyricist(i)
        
        Range(ColComposer & colIndex).Select
        ActiveCell.FormulaR1C1 = ListComposer(i)
        
        Range(ColAuthor & colIndex).Select
        ActiveCell.FormulaR1C1 = ListAuthor(i)
        
        Range(ColPublisher & colIndex).Select
        ActiveCell.FormulaR1C1 = ListPublisher(i)
        
        Range(ColDedicatee & colIndex).Select
        ActiveCell.FormulaR1C1 = ListDedicatee(i)
        
        Range(ColDate & colIndex).Select
        ActiveCell.FormulaR1C1 = ListDate(i)
        
        Range(ColSubject & colIndex).Select
        ActiveCell.FormulaR1C1 = ListSubject(i)
        
        Range(ColForm & colIndex).Select
        ActiveCell.FormulaR1C1 = ListForm(i)
        
        Range(ColKeySig & colIndex).Select
        ActiveCell.FormulaR1C1 = ListKeySig(i)
        
        Range(ColTimeSig & colIndex).Select
        ActiveCell.FormulaR1C1 = ListTimeSig(i)
        
        Range(ColTempo & colIndex).Select
        ActiveCell.FormulaR1C1 = ListTempo(i)
        
        Range(ColInstr & colIndex).Select
        ActiveCell.FormulaR1C1 = ListInstr(i)
        
        Range(ColCoverDesc & colIndex).Select
        ActiveCell.FormulaR1C1 = ListCoverDesc(i)
        
        Range(ColNote & colIndex).Select
        ActiveCell.FormulaR1C1 = ListNote(i)
        
        Range(ColPageCount & colIndex).Select
        ActiveCell.FormulaR1C1 = ListPageCount(i)
    Next
    ExportToCSV ("Data")
End Sub

Sub ExportToCSV(filename)
    Dim MyPath As String
    Dim MyFileName As String
    'The path and file names:
    MyPath = ActiveWorkbook.Path
    MyFileName = "Data" & Format(Date, "yyyy-mm-dd")
    'Makes sure the path name ends with "\":
    If Not Right(MyPath, 1) = "\" Then MyPath = MyPath & "\"
    'Makes sure the filename ends with ".csv"
    If Not Right(MyFileName, 4) = ".xlsx" Then MyFileName = MyFileName & ".xlsx"
    'Copies the sheet to a new workbook:
    Sheets("Data").Copy
    'The new workbook becomes Activeworkbook:
    With ActiveWorkbook
    'Saves the new workbook to given folder / filename:
        .SaveAs filename:= _
            MyPath & MyFileName, _
            FileFormat:=xlOpenXMLWorkbook, _
            CreateBackup:=False
    'Closes the file
        .Close False
    End With
End Sub
