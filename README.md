# Romantic-Era Lyrics

Many popular poets of the English Romantic-Era released their most famous works as scores of sheet music; this was because most of the people in their audiences could not afford to buy printed literature and instead had to grow familiar with the poets’ works through listening to them as live music. As printing became cheaper over time, we have forgotten the musical element of these works of art, focusing only on the written words.

Romantic-Era Lyrics endeavors to spark new interest in Romantic-Era Poetry by recreating the experience of the original audiences of these Romantic-Era poets in addition to providing an interactive scholarly database Romantic-Era Lyrics is a dynamic website that displays information about the scores, composers, poets, and publishers.

Initial data from the sheet music scores has been divided into five categories on the website. Each of these categories is searchable on a variety of categories:

-   *Scores* - the individual, specific pieces of sheet music
-   *Works* - the work of literature that a sheet music score represents. For example, there are four distinct sheet music scores for the work of literature "Auld Robin Gray"
-   *Collections* - collections of individual sheet music scores, such as books
-   *Publishers* - the publishing company that published a piece of sheet music or a collection of sheet music
-   *Artists* - the composers and poets behind the scores of music. Poets and composers are grouped under one category because many artists such as Caroline Norton functioned as both composer and poet for their publications.

Scores for which recordings are available also include Augmented Notes, a javascript application developed by Joanna Swafford of SUNY New Paltz that highlights measures on the sheet music as they are played.

The pages for the above categories are accessed through either the browse page or the search page.
