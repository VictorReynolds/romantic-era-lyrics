<?php
	/* This script is used to import .csv files into the Romantic-Era Lyrics Database.
	 * The .csv files must all be of individual pieces or for collections.
	 * The .csv files are assumed to be from a collection, so it is only necessary
	 * to specify if they are for individual pieces.
	 *
	 * Usage:
	 *
	 * php import_csv.php [ --individual ] file.csv [ collection_name ] [ collection_ref_id ]
	*/

	// error_reporting(E_ALL);
	// ini_set('display_errors', TRUE);
	// ini_set('display_startup_errors', TRUE);
	header('Content-Type: text/html; charset=utf-8');
	require_once('../conf/config.php');

	function formatCSVData($data_file_string)
	{
		$csv_data = str_getcsv($data_file_string, "\n");
		//foreach ($csv_data as &$datum)
		for ($i = 0; $i < count($csv_data); $i++)
		{
			$csv_data[$i] = str_getcsv($csv_data[$i]);

			// var_dump(array_filter($csv_data[$i]));
			// if (count(array_filter($csv_data[$i])) == 0)
			// {
			// 	unset($csv_data[$i]);
			// 	$i--;
			// }
		}
		formatCSVArrayKeys($csv_data);

		return $csv_data;
	}

	/*
	* This function returns an array containing the values of a specific field for
	* all of the rows in the input &$csv. This is used to figure out foreign keys.
	*
	* Parameters:
	*  $field - the field the array will be made from. e.g. "title"
	*  &$csv - the csv array (two-dimensional array) that we pull the list from
	*  $def_index - (default 0) the index of the definition array for the fields
	*/
	function listFromField($field, &$csv, $def_index = 0)
	{
		//$key_field = array_search($field, $csv[$def_index]);
		$array_field = array();
		for ($i = $def_index + 1; $i<count($csv); $i++)
		{
			array_push($array_field, $csv[$i][$field]);
		}
		return $array_field;
	}

	/*
	* This function takes a string array, looks at each index, and
	* splits that index into multiple indices based on the delimiter $delim
	*
	* Parameters:
	*  $arr - the array to be split apart
	*  $delim - (default ';') the delimiter
	*/
	function explodeArrayIndices(&$arr, $delim = ";", $trim = true)
	{
		for ($i = 0; $i < count($arr); $i++)
		{
			$index_vals = explode($delim, $arr[$i]);
			if (count($index_vals) > 1)
			{
				array_splice($arr, $i, 1, $index_vals);
			}
		}
	}

	/*
	* This function performs a number of functions that sets up a 
	* list of values to be added to a SQL table
	* It trims, each index, dedupes the array, and removes empty indices
	*
	* Parameters:
	*  $arr - the array to be formatted
	*  $delim - (default ';') the delimiter within array indices
	*/
	function formatArrayForInput(&$arr, $explode = true, $delim = ";")
	{
		if ($explode)
		{
			explodeArrayIndices($arr, $delim);
		}
		$arr = array_map('trim', $arr);
		$arr = array_unique($arr);
		$arr = array_filter($arr);
	}
	
    function formatCSVArrayKeys(&$arr)
	{
		for ($i = 1; $i < count($arr); $i++)
		{
			$count = count($arr[$i]);
			for ($j = 0; $j < $count; $j++)
			{
				$oldkey = $arr[0][$j];
				$arr[$i][$oldkey] = $arr[$i][$j];
				unset($arr[$i][$j]);
			}
		}
	}

	$titles = array("Dr.", "Mr.", "Mrs.", "Sir", "Lord", "Miss");
	$suffixes = array("Jr.", "Sr.", "Esq.", "Esqr.");
	function getTitleOrSuffixInName(&$name, $titleList, &$running_list)
	{
		foreach ($titleList as $title)
		{
			if (strpos($name, $title) !== false)
			{
				array_push($running_list, $title);
				$name = str_replace($title, "", $name);
			}
		}
	}

	function explodeName($fullName)
	{
		global $titles, $suffixes;

		$returnArray = array();

		$artistTitles = array();
		$artistSuffixes = array();
		getTitleOrSuffixInName($fullName, $titles, $artistTitles);
		getTitleOrSuffixInName($fullName, $suffixes, $artistSuffixes);

		$returnArray["title"] = $artistTitles;
		$returnArray["suffix"] = $artistSuffixes;

		$nameArr = explode(",", $fullName);

		if (count($nameArr) == 1)
		{
			$returnArray["l_name"] = trim($nameArr[0]);
			$returnArray["f_name"] = null; // TODO: fix this
		}
		elseif (count($nameArr) > 1)
		{
			$returnArray["l_name"] = trim($nameArr[0]);
			$returnArray["f_name"] = trim($nameArr[1]);
		}

		return $returnArray;
	}

	//$data should be the result of explodeName
	function getOrCreateArtistID($data)
	{
		if (gettype($data) == "string")
		{
			$data = explodeName($data);
		}

		global $db;
		$find_sql = 'SELECT id, title, suffix FROM artist WHERE l_name=:l_name AND IFNULL(f_name,"")=:f_name';
		$result = $db->prepare($find_sql);
		$result->execute(array("l_name"=>$data["l_name"],"f_name"=>$data["f_name"]));
		$resultList = $result->fetchAll(PDO::FETCH_ASSOC);
		if (empty($resultList)) {
			$insert_sql = 'INSERT INTO artist (l_name, f_name, title, suffix) VALUES (:l_name, :f_name, :title, :suffix)';
			$update = $db->prepare($insert_sql);
			$suffixImplode = implode($data["suffix"], ", ");
			$titleImplode = implode($data["title"], ", ");
			$update->execute(array(":l_name"=>$data["l_name"], ":f_name"=>$data["f_name"], ":title"=>$titleImplode, ":suffix"=>$suffixImplode));

			$id = $db->query("SELECT LAST_INSERT_ID();");
			$id = $id->fetch(PDO::FETCH_ASSOC);
			return $id["LAST_INSERT_ID()"];
		}
		else
		{
			//echo("Found id " . $resultList[0]["id"] . " for " . $data["l_name"] . ", " . $data["f_name"] . "<br>");
			//TODO: update suffixes and prefixes when new data is passed in
			return $resultList[0]["id"];
		}
	}

	function explodePublisherName($publisher)
	{
		$publisher = explode(":", $publisher);
		if (count($publisher) > 1) {
			$publisher["city"] = trim($publisher[0]);
			$publisher["name"] = trim($publisher[1]);
			unset($publisher[0]);
			unset($publisher[1]);
		}
		else {
			$publisher["name"] = trim($publisher[0]);
			$publisher["city"] = null;
			unset($publisher[0]);
		}

		return $publisher;
	}

	function getOrCreatePublisherID($publisher)
	{
		global $db;

		$publisher = explodePublisherName($publisher);

		$find_sql = 'SELECT id FROM publisher WHERE name=:name AND IFNULL(city,"")=:city';
		$result = $db->prepare($find_sql);
		$result->execute(array("name"=>$publisher["name"],"city"=>$publisher["city"]));
		$resultList = $result->fetchAll(PDO::FETCH_ASSOC);
		if (empty($resultList)) {
			$insert_sql = 'INSERT INTO publisher (name, city) VALUES (:name, :city)';
			$update = $db->prepare($insert_sql);
			$update->execute(array(":name"=>$publisher["name"], ":city"=>$publisher["city"]));

			$id = $db->query("SELECT LAST_INSERT_ID();");
			$id = $id->fetch(PDO::FETCH_ASSOC);
			return $id["LAST_INSERT_ID()"];
		}
		else
		{
			//echo("Found id " . $resultList[0]["id"] . " for " . $data["l_name"] . ", " . $data["f_name"] . "<br>");
			return $resultList[0]["id"];
		}
	}

	function getOrCreateWorkID($title)
	{
		global $db;

		$find_sql = 'SELECT id FROM work WHERE title=:title';
		$result = $db->prepare($find_sql);
		$result->execute(array(":title"=>$title));
		$resultList = $result->fetchAll(PDO::FETCH_ASSOC);
		if (empty($resultList)) {
			$insert_sql = 'INSERT INTO work (title) VALUES (:title)';
			$update = $db->prepare($insert_sql);
			$update->execute(array(":title"=>$title));

			$id = $db->query("SELECT LAST_INSERT_ID();");
			$id = $id->fetch(PDO::FETCH_ASSOC);
			return $id["LAST_INSERT_ID()"];
		}
		else
		{
			//echo("Found id " . $resultList[0]["id"] . " for " . $data["l_name"] . ", " . $data["f_name"] . "<br>");
			return $resultList[0]["id"];
		}
	}

	function semicolonToComma($str)
	{
		$str = explode(";", $str);
		if (count($str) > 1) {
			array_map("trim", $str);
			$str = implode($str, ",");
		}
		else
		{
			$str = $str[0];
		}

		return $str;
	}

	function addArtistScoreMapping($type, $artists, $score_id)
	{
		global $db;

		$artists = explode(";", $artists);
		$artists = array_map("trim", $artists);
		foreach ($artists as $artist)
		{
			if (!empty($artist))
			{
				$artist_id = getOrCreateArtistID($artist);
				$labels = array(":artist_id"=>$artist_id, ":score_id"=>$score_id, ":type"=>$type);

				$find_sql = 'SELECT score_id FROM artist_has_score WHERE artist_id=:artist_id AND score_id=:score_id AND type=:type;';
				$result = $db->prepare($find_sql);
				$result->execute($labels);
				$result = $result->fetchAll(PDO::FETCH_ASSOC);

				if (empty($result)) {
					$insert_sql = "INSERT INTO artist_has_score (artist_id, score_id, type) VALUES (:artist_id, :score_id, :type);";
					$query = $db->prepare($insert_sql);
					$query->execute($labels);
				}
			}
		}
	}

	function addArtistWorkMapping($artists, $work_id)
	{
		global $db;

		$artists = explode(";", $artists);
		$artists = array_map("trim", $artists);
		foreach ($artists as $artist)
		{
			if (!empty($artist))
			{
				$artist_id = getOrCreateArtistID($artist);
				$labels = array(":artist_id"=>$artist_id, ":work_id"=>$work_id);

				$find_sql = 'SELECT artist_id FROM artist_has_work WHERE artist_id=:artist_id AND work_id=:work_id';
				$result = $db->prepare($find_sql);
				$result->execute($labels);
				$result = $result->fetchAll(PDO::FETCH_ASSOC);

				if (empty($result)) {
					$insert_sql = "INSERT INTO artist_has_work (artist_id, work_id) VALUES (:artist_id, :work_id);";
					$query = $db->prepare($insert_sql);
					$query->execute($labels);
				}
			}
		}
	}

	function addScorePublisherMapping($publishers, $score_id)
	{
		global $db;
		$publishers = explode(";", $publishers);
		$publishers = array_map("trim", $publishers);

		foreach ($publishers as $publisher) {
			$publisher_id = getOrCreatePublisherID($publisher);

			$labels = array(":publisher_id"=>$publisher_id, ":score_id"=>$score_id);

			$find_sql = 'SELECT score_id FROM publisher_has_score WHERE publisher_id=:publisher_id AND score_id=:score_id';
			$result = $db->prepare($find_sql);
			$result->execute($labels);
			$result = $result->fetchAll(PDO::FETCH_ASSOC);

			if (empty($result)) {
				$insert_sql = "INSERT INTO publisher_has_score (publisher_id, score_id) VALUES (:publisher_id, :score_id);";
				$query = $db->prepare($insert_sql);
				$query->execute($labels);
			}
		}
	}

	function addCollectionPublisherMapping($publishers, $collection_id)
	{
		global $db;
		$publishers = explode(";", $publishers);
		$publishers = array_map("trim", $publishers);

		foreach ($publishers as $publisher) {
			$publisher_id = getOrCreatePublisherID($publisher);

			$labels = array(":publisher_id"=>$publisher_id, ":collection_id"=>$collection_id);

			$find_sql = 'SELECT publisher_id FROM publisher_has_collection WHERE publisher_id=:publisher_id AND collection_id=:collection_id';
			$result = $db->prepare($find_sql);
			$result->execute($labels);
			$result = $result->fetchAll(PDO::FETCH_ASSOC);

			if (empty($result)) {
				$insert_sql = "INSERT INTO publisher_has_collection (publisher_id, collection_id) VALUES (:publisher_id, :collection_id);";
				$query = $db->prepare($insert_sql);
				$query->execute($labels);
			}
		}
	}

	// Parameters:
	//   $score - should be a row from $csv_data representing
	$nonScoreFields = array("work_title", "lyricist", "composer", "publisher", "author", "subject", "index_start");
	function addScore($score, $collectionId = null)
	{
		// Start with work title, create one to one mapping to a work.
		// If the work_id isn't defined, then just use the score's title
		if (trim($score["work_id"] . "") == "")
		{
			$score["work_id"] = getOrCreateWorkID($score["title"]);
		}
		else
		{
			$score["work_id"] = getOrCreateWorkID($score["work_title"]);
		}

		global $nonScoreFields;
		global $db;

		// The score's ref_id should only matter if it's an individual piece of music not part of a collection.
		if ($score["ref_id"] != "")
		{
			$find_sql = "SELECT id FROM score WHERE ref_id=:ref_id";
			$result = $db->prepare($find_sql);
			$result->execute(array(":ref_id"=>$score["ref_id"]));
			$result = $result->fetchAll(PDO::FETCH_ASSOC);
		}

		if ($score["ref_id"] != "" && count($result) > 0) {
			// TODO: update score with ref_id to change data.
			echo("A score with the ref_id " . $score["ref_id"] . " already exists!");
		}
		else {
			$sql_fields = array();
			$sql_vals = array();
			foreach (array_keys($score) as $field)
			{
				if (!in_array($field, $nonScoreFields))
				{
					array_push($sql_fields, $field);
				}
			}

			$setField = function($str) { return ":field" . $str; };
			$setVal = function($str) { return ":val" . $str; };

			$insert_sql = "INSERT INTO score (" . implode($sql_fields, ", ") . ") VALUES (" . implode(array_map($setVal, $sql_fields), ", ") . ");";
			$insert = $db->prepare($insert_sql);

			$labelArr = array();
			foreach ($sql_fields as $field) {
				//$labelArr[$setField($field)] = $field;
				$labelArr[$setVal($field)] = $score[$field];
			}

			var_dump($labelArr);
			var_dump($insert_sql);
			$insert->execute($labelArr);
			$insert->debugDumpParams();
			$id = $db->query("SELECT LAST_INSERT_ID();");
			$id = $id->fetch(PDO::FETCH_ASSOC);
			$id = $id["LAST_INSERT_ID()"];

			addArtistScoreMapping("composer", $score["composer"], $id);
			addArtistWorkMapping($score["lyricist"], $score["work_id"]);
			addArtistWorkMapping($score["author"], $score["work_id"]);
			//createArtistScoreMapping("author", $score["author"]);
			addScorePublisherMapping($score["publisher"], $id);

			return $id;
		}
	}

	// Process:
	// Import CSV file
	// Map the header index keys to each row
	// Sort out anything where there will be a mapping
	// Work is the only one to many relationship
	// Lyricist, Composer, and Author have many to many with the artist table
	// Publisher is many to many
	// Separate lists out for the above fields and add them to their appropriate
	// tables, preventing duplicates based on the names.
	// 
	// Now, start to import the scores
	// For the fields I mentioned above, add the appropriate mappings
	// to the mapping tables
	// This will be the complex part - multiple select and inserts will need to be run
	// Unset values for each row that do not correspond to actual fields
	// in the score table
	// Add work_id field and set appropriately
	// Format any values that aren't formatted correctly.
	// Finally, add each row to the score table and set the appropriate values


	/*
	* This is a function for adding a list of separate scores from a CSV
	* 
	* Parameters:
	* 
	*/
	function addScoresFromList($listName)
	{
		$data_file_string = file_get_contents($listName);

		//Split the CSV into a map
		$csv_data = formatCSVData($data_file_string);

		// $array_title = listFromField("work_title", $csv_data);
		// formatArrayForInput($array_title, false);

		// $array_composer = listFromField("composer", $csv_data);
		// formatArrayForInput($array_composer);

		// $array_lyricist = listFromField("lyricist", $csv_data);
		// formatArrayForInput($array_lyricist);

		// //$array_author = listFromField("author", $csv_data);
		// //formatArrayForInput($array_author);

		// $array_publisher = listFromField("publisher", $csv_data);
		// formatArrayForInput($array_publisher);

		echo "<pre>";
		// foreach($array_composer as $artist)
		// {
		// 	  getOrCreateArtistID($artist, "composer");
		// }
		//var_dump($csv_data);
			foreach($csv_data as $index=>$item)
			{
				if ($index > 0) {
					addScore($item);
				}
			}
		echo "</pre>";
	}

	function addScoresCollection($collectionFile, $collectionTitle, $collectionRefId)
	{
		global $db;

		echo "<pre>";
		$data_file_string = file_get_contents($collectionFile);

		$csv_data = formatCSVData($data_file_string);

		// Select the year from the second piece of sheet music in the data
		// The easiest way for me to get the year right now
		$labels = array(":title"=>$collectionTitle, ":year"=>$csv_data[1]["year"], ":ref_id"=>$collectionRefId);

		$insert_sql = "INSERT INTO collection (title, year, ref_id) VALUES (:title, :year, :ref_id);";
		$query = $db->prepare($insert_sql);
		$query->execute($labels);
		$collection_id = $db->query("SELECT LAST_INSERT_ID();");
		$collection_id = $collection_id->fetch(PDO::FETCH_ASSOC);
		$collection_id = $collection_id["LAST_INSERT_ID()"];

		foreach($csv_data as $index=>$item)
		{
			// Make sure this isn't the header row and that the row isn't empty
			if ($index > 0 && count(array_filter($item)) > 0)
			{
				$score_id = addScore($item);

				$labels = array(":collection_id"=>$collection_id, ":score_id"=>$score_id, ":index_start"=>$item["index_start"]);

				$insert_sql = "INSERT INTO collection_has_score (collection_id, score_id, index_start) VALUES (:collection_id, :score_id, :index_start);";
				$query = $db->prepare($insert_sql);
				$query->execute($labels);
			}
		}

		addCollectionPublisherMapping($csv_data[1]["publisher"], $collection_id);

			//var_dump($csv_data);
		echo "</pre>";
	}

	// addScoresCollection("./CSV/bronte_sonnets.csv", "Bronte's Sonnets", "bronte_sonnets");
	// addScoresCollection("./CSV/norton_ten.csv", "Ten Things by Caroline Norton", "norton_ten");
	// addScoresCollection("./CSV/scottishminstrel_volfivesix.csv", "Scottish Minstrels Volumes Five and Six", "scottishminstrel_volfivesix");
	// addScoresCollection("./CSV/scottishminstrel_volonetwo.csv", "Scottish Minstrels Volumes One and Two", "scottishminstrel_volonetwo");
	// addScoresCollection("./CSV/scottishminstrel_volthreefour.csv", "Scottish Minstrels Volumes Three and Four", "scottishminstrel_volthreefour");
	// addScoresCollection("./CSV/shelley_poems.csv", "Three Poems by Shelley", "shelley_poems");
	// addScoresCollection("./CSV/thynne.csv", "Thynne", "thynne");
	// addScoresCollection("./tennyson_maud.csv", "Tennyson's Maud", "tennyson_maud");
	// addScoresFromList("test.csv");
	// addScoresFromList("hemansmetadata.csv");

	if (count($argv) < 2)
	{
		echo "Please provide at least one file to add to the database.";
		exit;
	}

	if ($argv[1] == "--individual")
	{
		if (count($argv) < 3)
		{
			echo "Please provide a file to add to the database.";
			exit;
		}

		if (file_exists($argv[2]))
		{
			echo "Importing " . $argv[2] . "\n".
			addScoresFromList($argv[2]);
			echo "Successfully added csv file " . $argv[2] . " to the database.\n";
		}
	}
	else
	{
		if (count($argv) < 4)
		{
			echo "Please provide a collection name and reference id.";
		}
		elseif (file_exists($argv[1]))
		{
			echo "Importing " . $argv[1] . "\n".
			addScoresCollection($argv[1], $argv[2], $argv[3]);
			echo "Successfully added csv file " . $argv[1] . " to the database.\n";
		}
	}

?>