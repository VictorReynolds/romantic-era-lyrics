<?php
	define("BASE_URL", "/");
	if ($_SERVER)
	{
		define("ROOT_PATH", $_SERVER["DOCUMENT_ROOT"] . "/lyrics/");
	}

	define("DB_HOST", "localhost");
	define("DB_NAME", "reldb");
	define("DB_PORT", "3306");
	define("DB_USER", "root");
	define("DB_PASS", "");

	define("IMG_ROOT", "/lyrics/score_images/");

	try {
		$db = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";port=" . DB_PORT, DB_USER, DB_PASS);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$db->exec("SET NAMES 'utf8'");
	}
	catch (Exception $e) {
		echo "Could not connect to the database.";
		exit;
	}
?>
